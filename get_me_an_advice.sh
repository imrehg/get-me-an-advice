#!/bin/bash

# Exit if error occurs
set -e

# Colorization
COLOR="\e[3$(( $RANDOM * 6 / 32767 + 1 ))m"
NC='\033[0m' # No Color

REPLY=$(curl --retry 10 --silent https://api.adviceslip.com/advice | jq -r .slip.advice)

echo -e "${COLOR}${RED}${REPLY}${NC}"
