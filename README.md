# get me an advice

A small script that queries the [AdviceSlip API](https://api.adviceslip.com/)
for an advice, and then prints it out in glorious colour.

## Usage

Run in your shell with `./get_me_an_advice.sh`
